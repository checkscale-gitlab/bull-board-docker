const { setQueues, BullAdapter, router } = require('bull-board')
const Queue = require('bull')
const redis = require('redis')
const app = require('express')()

const PORT = 80;
const {
  REDIS_HOST,
  REDIS_PORT,
  REDIS_PASSWORD,
  REDIS_USE_TLS,

  BULL_PREFIX,
  REFRESH_INTERVAL,
} = process.env

const redisOptions = {
  redis: {
    port: REDIS_PORT || 6379,
    host: REDIS_HOST || 'localhost',
    password: REDIS_PASSWORD || undefined,
    tls: REDIS_USE_TLS || false,
  },
}

const client = redis.createClient(redisOptions.redis)

let queue_names = []

const prefix = BULL_PREFIX || 'bull'

function refreshQueues() {
  console.log('Refreshing Queues')
  client.KEYS(`${prefix}:*`, (_err, keys) => {
    keys.map(key => {
      const queue_name = key.replace(/^.+?:(.+?):.+?$/, '$1')
      if (!queue_names.includes(queue_name)) {
        const queue = new Queue(queue_name, redisOptions);
        const queueAdapter = new BullAdapter(queue);
        setQueues([queueAdapter])
        queue_names.push(queue_name)
      }
    })
  })
}

const run = () => {
  setInterval(refreshQueues, REFRESH_INTERVAL || 10000)

  refreshQueues()

  app.use('/', router)
  const server = app.listen(PORT, () => {
    console.log(`Running on ${PORT}...`)
    console.log(`For the UI, open http://localhost:${PORT}/`)
  })
  process.on('SIGINT', () => {
    server.close(() => {
      process.exit();
    });
  });
}

run()
