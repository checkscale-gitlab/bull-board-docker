FROM node:14-alpine AS build
ENV NODE_ENV production
WORKDIR /app
COPY package*.json ./
RUN npm ci --only=production

FROM node:14-alpine
ENV NODE_ENV production
EXPOSE 80
RUN apk add dumb-init
USER node
WORKDIR /app
COPY --chown=node:node --from=build /app ./
COPY --chown=node:node . ./

CMD  ["dumb-init", "node", "src/index.js"]
